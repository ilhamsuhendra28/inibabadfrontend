function showlatestvideo(menu, url){
	$.ajax({
		type: "POST",
		url: url,
		dataType: "html",
		data: {
			menu:menu
		},
		beforeSend: function() {
			$("#loaderlatestvideo").html('<div class="loader-box"><div class="loader-30"></div></div>');
			$("#datalatestvideo").html('');
		},
		success: function(data){
			$("#datalatestvideo").html(data);
		},
		complete: function(){
			$("#loaderlatestvideo").html('');				
		},
		error: function (xhr, ajaxOptions, thrownError) {
			var pesan = xhr.status + " " + thrownError + "\n" +  xhr.responseText;
			$("#datalatestvideo").html('<div class="alert alert-danger dark" role="alert"><i class="icofont icofont-warning-alt"></i> '+pesan+'</div>');
		},
	});
}

function showpopulerpost(menu, url){
	$.ajax({
		type: "POST",
		url: url,
		dataType: "html",
		data: {
			menu:menu
		},
		beforeSend: function() {
			$("#loaderpopulerpost").html('<div class="loader-box"><div class="loader-30"></div></div>');
			$("#datapopulerpost").html('');
		},
		success: function(data){
			$("#datapopulerpost").html(data);
		},
		complete: function(){
			$("#loaderpopulerpost").html('');				
		},
		error: function (xhr, ajaxOptions, thrownError) {
			var pesan = xhr.status + " " + thrownError + "\n" +  xhr.responseText;
			$("#datapopulerpost").html('<div class="alert alert-danger dark" role="alert"><i class="icofont icofont-warning-alt"></i> '+pesan+'</div>');
		},
	});
}

function showcorona(negara, url){
	$.ajax({
		type: "POST",
		url: url,
		dataType: "html",
		data: {
			negara:negara
		},
		beforeSend: function() {
			$("#loadercorona").html('<div class="loader-box"><div class="loader-30"></div></div>');
			$("#datacorona").html('');
		},
		success: function(data){
			$("#datacorona").html(data);
		},
		complete: function(){
			$("#loadercorona").html('');				
		},
		error: function (xhr, ajaxOptions, thrownError) {
			var pesan = xhr.status + " " + thrownError + "\n" +  xhr.responseText;
			$("#datacorona").html('<div class="alert alert-danger dark" role="alert"><i class="icofont icofont-warning-alt"></i> '+pesan+'</div>');
		},
	});
}