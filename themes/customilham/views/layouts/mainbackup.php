<?php
	use yii\helpers\Url;
	use app\components\Logic;
	use app\assets\AppAsset;

	AppAsset::register($this);
?>

<?php $this->beginPage() ?>
	<!DOCTYPE html>
	<html class="no-js" lang="id">
		<head>
			<meta charset="utf-8">
			<meta http-equiv = "content-language" content = "id">
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
			<link rel="canonical" href="<?php echo Yii::$app->request->url; ?>">
			<script async src="https://www.googletagmanager.com/gtag/js?id=UA-67751236-3"></script>
			<script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());

				gtag('config', 'UA-67751236-3');
			</script>
			
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<script>
				(adsbygoogle = window.adsbygoogle || []).push({
				google_ad_client: "ca-pub-6606907698073340",
				enable_page_level_ads: true
				});
			</script>
			<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $this->theme->baseUrl; ?>/assets/imgs/apple-icon-57x57.png">
			<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $this->theme->baseUrl; ?>/assets/imgs/apple-icon-60x60.png">
			<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $this->theme->baseUrl; ?>/assets/imgs/apple-icon-72x72.png">
			<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->theme->baseUrl; ?>/assets/imgs/apple-icon-76x76.png">
			<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $this->theme->baseUrl; ?>/assets/imgs/apple-icon-114x114.png">
			<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $this->theme->baseUrl; ?>/assets/imgs/apple-icon-120x120.png">
			<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $this->theme->baseUrl; ?>/assets/imgs/apple-icon-144x144.png">
			<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $this->theme->baseUrl; ?>/assets/imgs/apple-icon-152x152.png">
			<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->theme->baseUrl; ?>/assets/imgs/apple-icon-180x180.png">
			<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $this->theme->baseUrl; ?>/assets/imgs/android-icon-192x192.png">
			<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->theme->baseUrl; ?>/assets/imgs/favicon-32x32.png">
			<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $this->theme->baseUrl; ?>/assets/imgs/favicon-96x96.png">
			<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->theme->baseUrl; ?>/assets/imgs/favicon-16x16.png">
			
			<?php $this->head() ?>
		</head>

		<body>
			<?php $this->beginBody() ?>
			<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59dcc5b50751684f"></script> 
			<div class="addthis_inline_follow_toolbox collapse navbar-collapse"></div>
			
			<div id="loadmodal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog"
			 aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="myLargeModalLabel">Large modal</h4>
							<button class="close" type="button" data-dismiss="modal" aria-label="Close" data-original-title="" title=""><span aria-hidden="true"><span aria-hidden="true"><i class="icofont icofont-ui-close"></i></span></span></button>
						</div>
						<div id="loadermodal"></div>
						<div class="modal-body" id="datamodal"></div>
					</div>
				</div>
			</div>
			
			<div class="main-wrap">
				<aside id="sidebar-wrapper" class="custom-scrollbar offcanvas-sidebar position-right"></aside>
				
				<header class="main-header header-style-2 mb-20">
					<div class="header-bottom header-sticky background-white text-center">
						<div class="float-right">
							<div class="float-left mobile_menu d-lg-none d-block"></div>
							<div class="float-right search_google d-lg-none d-block"></div>
						</div>
						<div class="scroll-progress gradient-bg-1"></div>
						<div class="container">
							<div class="row">
								<div class="col-lg-1 col-sm-2">
									<div class="header-logo d-none d-lg-block">
										<a href="<?php echo Url::home(); ?>">
											<img style="height: 60px;width: 120px;vertical-align: top;max-width: max-content;" class="logo-img d-inline" src="<?php echo $this->theme->baseUrl; ?>/assets/imgs/logoinibabad.png" alt="WEB - INIBABAD - JERNIH MEMANDANG SEKITAR">
										</a>
									</div>
									<div class="logo-tablet d-md-inline d-lg-none d-none">
										<a href="<?php echo Url::home(); ?>">
											<img style="height: 60px;width: 120px;vertical-align: top;max-width: max-content;" class="logo-img d-inline" src="<?php echo $this->theme->baseUrl; ?>/assets/imgs/logoinibabad.png" alt="TABLET - INIBABAD - JERNIH MEMANDANG SEKITAR">
										</a>
									</div>
									<div class="logo-mobile d-block d-md-none">
										<a href="<?php echo Url::home(); ?>">
											<img style="height: 60px;width: 120px;vertical-align: top;max-width: max-content;" class="logo-img d-inline" src="<?php echo $this->theme->baseUrl; ?>/assets/imgs/logoinibabad.png" alt="MOBILE - INIBABAD - JERNIH MEMANDANG SEKITAR">
										</a>
									</div>
								</div>
								<div class="col-lg-11 col-md-11 main-header-navigation">
									<div class="main-nav text-left float-lg-left float-md-right">
										<?php 
											$menu = Logic::hasMenu(); 
											$controllerparent = Yii::$app->controller->id;
											$actionparent = Yii::$app->controller->action->id;
											if($controllerparent == 'site' && $actionparent == 'index'){
												$classactive = 'active';
											}
										?>
										<nav>
											<ul class="mobi-menu main-menu d-none d-lg-inline" id="navigation">
												<li class="<?php echo $classactive; ?>">
													<a href="<?php echo Url::home(); ?>">
														<i class="icofont icofont-home"></i> Home
													</a>
												</li>
												<?php 
													foreach($menu as $mdx=>$mrow){ 
														if(empty($mrow['child'])){
															$liclassparent = '';
															$liurlparent = Url::to(['/list/index', 'name'=>$mrow['slug_name']]);
														}else{
															$liclassparent = 'menu-item-has-children';
															$liurlparent = 'javascript:void(0)';
														}
														if(strtolower($mrow['slug_name']) == $_GET['name']){
															$classactive = 'active';
														}else{
															$classactive = '';
														}
														
														if(!empty($mrow['child'])){
															foreach($mrow['child'] as $cdx=>$crow){
																if(strtolower($crow['slug_name']) == $_GET['name']){
																	$classactive = 'active';
																}
															}	
														}
												?>
												<li class="<?php echo $liclassparent; ?> <?php echo $classactive; ?>">
													<a href="<?php echo $liurlparent; ?>">
														<span style="display:none;">Menu - </span><?php echo $mrow['data'];?> <?php echo $mrow['name']; ?>
													</a>
													<?php
														if(!empty($mrow['child'])){
													?>
													
															<ul class="sub-menu text-muted font-small">
													<?php	
																foreach($mrow['child'] as $cdx=>$crow){
																	if(strtolower($crow['slug_name']) == $_GET['name']){
																		$classactive = 'active';
																	}else{
																		$classactive = '';
																	}
																	$liurlchild = Url::to(['/list/index', 'name'=>$crow['slug_name']]);
													?>
																	<li class="<?php echo $classactive; ?>">
																		<a href="<?php echo $liurlchild; ?>"><span style="display:none;"> Menu - </span><?php echo $crow['data'];?> <?php echo $crow['name']; ?></a>
																	</li>
													<?php
																}
													?>
															</ul>
													
													<?php
														}	
													?>
												</li>
												<?php } ?>
											</ul>
										</nav>
									</div>
									<div class="search-form d-lg-inline float-right position-relative d-none" id="navi_search">
										<script async src="https://cse.google.com/cse.js?cx=35cae123ea1de9734"></script>
										<div class="gcse-search"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</header>
				
				<main class="position-relative">
					<div class="container">
						<?= $content; ?>
					</div>
				</main>
				
				<footer>
					<div class="footer-area pt-20 bg-white">
						<div class="container">
							<div class="row pb-30 text-center">
								<div class="col-lg-12">
									<a target="_blank" href="https://instagram.com/inibabadcom" title="Ikuti kami di Instagram"><i class="icofont icofont-social-instagram icofont-2x"></i></a>
									<a target="_blank" href="https://facebook.com/babad.official" title="Ikuti kami di Facebook"><i class="icofont icofont-social-facebook icofont-2x"></i></i></a>
									<a target="_blank" href="https://twitter.com/inibabadcom" title="Ikuti kami di Twitter"><i class="icofont icofont-social-twitter icofont-2x"></i></i></a>
								</div>
							</div>
						</div>
					</div>
					
					<div class="footer-bottom-area bg-white text-muted text-center">
						<div class="container">
							<div class="footer-border pt-20 pb-20">
								<div class="row d-flex align-items-center justify-content-between">
									<div class="col-12">
										<div class="footer-copy-right">
											<p class="font-small text-muted">© 2015 ASMI GROUP | All rights reserved</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</footer>
			</div>
			
			<div class="dark-mark"></div>
			<script id="dsq-count-scr" src="//https-inibabad-com.disqus.com/count.js" async></script>
			<?php $this->endBody() ?>
			<script>
				$.ajaxSetup({
					data: <?= \yii\helpers\Json::encode([
						\yii::$app->request->csrfParam => \yii::$app->request->csrfToken,
					]) ?>
				});
			</script>
		</body>
	</html>
<?php $this->endPage() ?>