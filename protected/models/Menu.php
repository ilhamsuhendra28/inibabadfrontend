<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property string $name
 * @property int|null $parent
 * @property string|null $route
 * @property int|null $order
 * @property string|null $data
 * @property int|null $is_frontend
 *
 * @property Menu $parent0
 * @property Menu[] $menus
 * @property TrArtikel[] $trArtikels
 * @property TrArtikelCopy1[] $trArtikelCopy1s
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['parent', 'order', 'is_frontend', 'is_video', 'is_image'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['route'], 'string', 'max' => 255],
            [['data'], 'string', 'max' => 100],
            [['parent'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['parent' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'parent' => 'Parent',
            'route' => 'Route',
            'order' => 'Order',
            'data' => 'Data',
            'is_frontend' => 'Is Frontend',
            'is_video' => 'Is Video',
            'is_image' => 'Is Image',
        ];
    }

    /**
     * Gets query for [[Parent0]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getParent0()
    {
        return $this->hasOne(Menu::className(), ['id' => 'parent']);
    }

    /**
     * Gets query for [[Menus]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['parent' => 'id']);
    }

    /**
     * Gets query for [[TrArtikels]].
     *
     * @return \yii\db\ActiveQuery|TrArtikelQuery
     */
    public function getTrArtikels()
    {
        return $this->hasMany(TrArtikel::className(), ['menu_id' => 'id']);
    }

    /**
     * Gets query for [[TrArtikelCopy1s]].
     *
     * @return \yii\db\ActiveQuery|TrArtikelCopy1Query
     */
    public function getTrArtikelCopy1s()
    {
        return $this->hasMany(TrArtikelCopy1::className(), ['menu_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return MenuQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MenuQuery(get_called_class());
    }
}
