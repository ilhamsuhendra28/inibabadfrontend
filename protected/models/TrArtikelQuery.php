<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TrArtikel]].
 *
 * @see TrArtikel
 */
class TrArtikelQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TrArtikel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TrArtikel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
