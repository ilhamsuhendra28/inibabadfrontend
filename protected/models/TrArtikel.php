<?php

namespace app\models;

use Yii;
use app\components\Logic;

/**
 * This is the model class for table "tr_artikel".
 *
 * @property int $id
 * @property int $menu_id
 * @property string $judul
 * @property string $isi
 * @property string $sub_isi
 * @property int|null $total_views
 * @property string|null $created_date
 * @property int|null $created_by
 *
 * @property Menu $menu
 * @property User $createdBy
 */
class TrArtikel extends \yii\db\ActiveRecord
{
	public $dataid;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tr_artikel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menu_id', 'judul', 'isi'], 'required'],
            [['menu_id', 'total_views', 'created_by'], 'integer'],
            [['isi', 'sub_isi'], 'string'],
            [['created_date'], 'safe'],
            [['judul'], 'string', 'max' => 255],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'judul' => 'Judul',
            'isi' => 'Isi',
            'sub_isi' => 'Sub Isi',
            'total_views' => 'Total Views',
            'created_date' => 'Created Date',
            'created_by' => 'Created By',
        ];
    }
	
	public function beforeSave($insert)
    {
		if(!empty($this->created_date)){
			$this->created_date = Logic::dbFormatDate($this->created_date);			
		}
	
        return parent::beforeSave($insert);
    }
	
	public function afterFind()
    {
        parent::afterFind();
		if(!empty($this->created_date)){
			$this->created_date = Logic::convertDate($this->created_date);			
		}
    }
	
    /**
     * Gets query for [[Menu]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * {@inheritdoc}
     * @return TrArtikelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrArtikelQuery(get_called_class());
    }
}
