<?php
	use Yii;
	use yii\helpers\Url;
	use app\components\Logic;
	
	$backgroundlist = Logic::gambarPertama($model->isi);
?>

<div class="loop-list-style-1">
	<article class="first-post p-10 background-white border-radius-10 mb-30 wow fadeIn animated">
		<div class="img-hover-slide border-radius-15 mb-10 position-relative overflow-hidden">
			<a href="<?php echo Url::to(['/content/index', 'judul'=>$model->slug_judul]); ?>">
				<div class="thumb-overlay img-hover-slide border-radius-15 position-relative" style="background-image: url(<?php echo $backgroundlist; ?>)"></div>
				<div style="display:none"><?php echo $model->menu->name; ?> - <?php echo $model->judul; ?></div>
			</a>
		</div>
		<div class="pr-10 pl-10">
			<h4 class="post-title mb-20">
				<span class="post-format-icon">
					<i class="icofont icofont-paper"></i>
				</span>
				<a href="<?php echo Url::to(['/content/index', 'judul'=>$model->slug_judul]); ?>"><span style="display:none;">Judul Editor - </span><?php echo $model->judul; ?></a>
			</h4>
			<div class="mb-20 overflow-hidden">
				<div class="entry-meta meta-1 font-x-small color-grey text-uppercase">
					<span class="post-by"><i class="icofont icofont-calendar"></i> <?php echo $model->created_date; ?></span>
					<span class="post-by"><i class="icofont icofont-eye"></i><?php echo NUMBER_FORMAT($model->total_views); ?></span>
					<span class="post-by"><i class="icofont icofont-comment"></i><a href="<?php echo Url::to(['/content/index', 'judul'=>$model->slug_judul]); ?>#disqus_thread">0 KOMENTAR</a></span>
				</div>
			</div>
			<hr class="wp-block-separator is-style-wide">
			<div class="entry-main-content">
				<p class="text-justify"><?php echo substr(Logic::removeNbsp(strip_tags($model->isi)), 0,150); ?>...</p>
			</div>
		</div>
		
		<div class="text-right">
			<h6 class="font-medium">
				<a class="btn btn-sm btn-pill btn-info" href="<?php echo Url::to(['/content/index', 'judul'=>$model->slug_judul]); ?>"><i class="icofont icofont-eye"></i> Baca selengkapnya <span style="display:none;"><?php echo $model->menu->name; ?> - <?php echo substr($model->judul,0,50); ?></span></a>
			</h6>
		</div>
	</article>
</div>