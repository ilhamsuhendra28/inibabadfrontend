<?php
	use yii\widgets\ListView;
	use yii\helpers\Url;
	$models = $dataProvider->getModels();
	$first = reset($models);
	
?>
<title><?php echo $menu->name.' - '.$first->judul; ?></title>
<span style="display:none"><h1><?php echo $menu->name.' - '.$first->judul; ?></h1></span>

<div class="row">
	<div class="col-sm-6">
		<div class="loop-list-style-1 background-white border-radius-10 mb-30 wow fadeIn animated">
			<article class="pl-10 pt-10 pr-10">
				<ins class="adsbygoogle"
					 style="display:block"
					 data-ad-client="ca-pub-6606907698073340"
					 data-ad-slot="8345490771"
					 data-ad-format="horizontal"
					 data-full-width-responsive="true"></ins>
					<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
			</article>
		</div>		
	</div>
	<div class="col-sm-6">
		<div class="loop-list-style-1 background-white border-radius-10 mb-30 wow fadeIn animated">
			<article class="pl-10 pt-10 pr-10">
				<ins class="adsbygoogle"
					 style="display:block"
					 data-ad-client="ca-pub-6606907698073340"
					 data-ad-slot="8345490771"
					 data-ad-format="horizontal"
					 data-full-width-responsive="true"></ins>
					<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
			</article>
		</div>		
	</div>
	
	<div class="col-lg-12 col-md-12 primary-sidebar sticky-sidebar sidebar-left order-1 order-md-1">
		<div class="latest-post mb-15">
			<div class="widget-header position-relative mb-10" style="border-bottom: 1px solid #d4cfcf;">
				<div class="row">
					<div class="col-12">
						<h4 class="widget-title mb-0"><i class="icofont icofont-dna-alt-1"></i> Perkembangan Virus <span>Corona</span></h4>
					</div>
				</div>
			</div>
			<div id="loadercorona"></div>
			<div class="post-aside-style-3 bg-white border-radius-15" id="datacorona"></div>
		</div>
	</div>
</div>

<?php if($menu->is_video != true){ ?>
	<div class="row">
		<div class="col-lg-3 col-md-3 primary-sidebar sticky-sidebar sidebar-left order-2 order-md-1">
			<?php echo $this->render('/site/_arsipberita', ['menu'=>$menu]); ?>
			
			<div class="widget-header position-relative mb-10" style="border-bottom: 1px solid #d4cfcf;">
				<div class="row">
					<div class="col-12">
						<h4 class="widget-title mb-0"><i class="icofont icofont-ui-video-play"></i> Video <span>Terbaru</span></h4>
					</div>
				</div>
			</div>
			<div id="loaderlatestvideo"></div>
			<div class="post-aside-style-3" id="datalatestvideo"></div>
		</div>
		<div class="col-lg-9 col-md-9 order-1 order-md-2">
			<div class="row">
				<div class="col-lg-8 col-md-12">
					<div class="latest-post mb-50">
						<div class="widget-header position-relative mb-10" style="border-bottom: 1px solid #d4cfcf;">
							<div class="row">
								<div class="col-9">
									<h4 class="widget-title mb-0"><?php echo $menu->data; ?> <?php echo $menu->name; ?></h4>
								</div>
							</div>
						</div>
						<?=
							ListView::widget([
								'dataProvider' => $dataProvider,
								'itemView' => '_listpost',
								'layout' => "{items}\n{pager}",
								'pager' => [
									'maxButtonCount' => 3,
								],
							]);
						?>
					</div>
				</div>
				<div class="col-lg-4 col-md-12 primary-sidebar sticky-sidebar sidebar-right order-2 order-md-1">
					<div class="sidebar-widget mb-30">
						<div class="widget-header position-relative mb-10" style="border-bottom: 1px solid #d4cfcf;">
							<div class="row">
								<div class="col-12">
									<h4 class="widget-title mb-0">Postingan <span>terpopuler</span></h4>
								</div>
							</div>
						</div>
						<div id="loaderpopulerpost"></div>
						<div class="post-aside-style-3 bg-white border-radius-15" id="datapopulerpost"></div>
					</div>	
				</div>	
			</div>
		</div>	
	</div>
	
	<script>
		$(document).ready(function() {
			showlatestvideo('<?php echo $menu->slug_name;?>', '<?php echo Url::toRoute(['/site/latestvideo']); ?>');
			showpopulerpost('<?php echo $menu->slug_name;?>', '<?php echo Url::toRoute(['/site/populerpost']); ?>');
		});
	</script>
	
<?php }else{ ?>
	<div class="row">
		<div class="col-lg-12 col-md-12 order-1 order-md-2">
			<div class="latest-post mb-50">
				<div class="widget-header position-relative mb-10" style="border-bottom: 1px solid #d4cfcf;">
					<div class="row">
						<div class="col-9">
							<h4 class="widget-title mb-0"><?php echo $menu->data; ?> <?php echo $menu->name; ?></h4>
						</div>
					</div>
				</div>
			
				<?=
					ListView::widget([
						'dataProvider' => $dataProvider,
						'itemView' => '_listvideo',
						'layout' => "{items}<div class='col-lg-12'>{pager}</div>",
						'options' => ['class' => 'row'],
						'itemOptions'  => ['class' => "col-lg-3 col-sm-12"],
						'pager' => [
							'maxButtonCount' => 3,
						],
					]);
				?>
			</div>
		</div>	
	</div>
<?php } ?>

<div class="row">
	<div class="col-sm-6">
		<div class="loop-list-style-1 background-white border-radius-10 mb-30 wow fadeIn animated">
			<article class="pl-10 pt-10 pr-10">
				<ins class="adsbygoogle"
					 style="display:block"
					 data-ad-client="ca-pub-6606907698073340"
					 data-ad-slot="8345490771"
					 data-ad-format="horizontal"
					 data-full-width-responsive="true"></ins>
					<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
			</article>
		</div>		
	</div>
	<div class="col-sm-6">
		<div class="loop-list-style-1 background-white border-radius-10 mb-30 wow fadeIn animated">
			<article class="pl-10 pt-10 pr-10">
				<ins class="adsbygoogle"
					 style="display:block"
					 data-ad-client="ca-pub-6606907698073340"
					 data-ad-slot="8345490771"
					 data-ad-format="horizontal"
					 data-full-width-responsive="true"></ins>
					<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
			</article>
		</div>		
	</div>
</div>

<script>
	$(document).ready(function() {
		showcorona('Indonesia', '<?php echo Url::toRoute(['/site/corona']); ?>');
	});
</script>