<?php
	use Yii;
	use app\components\Logic;
?>


	<article class="bg-white border-radius-15 mb-30 p-10">
		<div class="post-thumb d-flex mb-15 border-radius-15 img-hover-scale">
			<iframe width="100%" height="auto" src="<?php echo Logic::videoPertama($model->isi);?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>
	</article>