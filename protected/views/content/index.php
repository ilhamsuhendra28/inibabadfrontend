<?php
	use Yii;
	use app\components\Logic;
	use yii\helpers\Url;
?>
<title><?php echo $artikel->judul; ?></title>
<span style="display:none"><h1><?php echo $artikel->judul; ?></h1></span>

<div class="row">
	<div class="col-lg-12 col-md-12 primary-sidebar sticky-sidebar sidebar-left order-1 order-md-1">
		<div class="latest-post mb-15">
			<div class="widget-header position-relative mb-10" style="border-bottom: 1px solid #d4cfcf;">
				<div class="row">
					<div class="col-12">
						<h4 class="widget-title mb-0"><i class="icofont icofont-dna-alt-1"></i> Perkembangan Virus <span>Corona</span></h4>
					</div>
				</div>
			</div>
			<div id="loadercorona"></div>
			<div class="post-aside-style-3 bg-white border-radius-15" id="datacorona"></div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 order-1 order-md-1">
		<div class="row">
			<div class="col-lg-8 col-md-12">
				<div class="latest-post mb-20 p-20 background-white border-radius-10">
					<h1 class="post-title mb-15">
						<?php echo $artikel->judul; ?>
                    </h1>
					<div class="entry-meta meta-1 font-x-small color-grey text-uppercase">
                        <div class="mb-5 overflow-hidden">
							<div class="entry-meta meta-1 font-x-small color-grey float-left text-uppercase">
								<span class="post-by"><i class="icofont icofont-calendar"></i> <?php echo $artikel->created_date; ?></span>
								<span class="post-by"><i class="icofont icofont-eye"></i><?php echo NUMBER_FORMAT($artikel->total_views); ?></span>
								<span class="post-by"><i class="icofont icofont-comment"></i><a href="<?php echo Yii::$app->request->url; ?>#disqus_thread">0 KOMENTAR</a></span>
							</div>
						</div>
                    </div>
					<div class="entry-meta meta-2">
						<span class="post-by"><div class="addthis_inline_share_toolbox"></div></span>
					</div>
					<div class="bt-1 border-color-1 mb-10"></div>
					<div class="entry-main-content">
						<?php echo Logic::removeNbsp($artikel->isi); ?>
					</div>
					<div id="disqus_thread"></div>
					<script>
						(function() {
							var d = document, s = d.createElement('script');
							
							s.src = 'https://https-inibabad-com.disqus.com/embed.js';
							
							s.setAttribute('data-timestamp', +new Date());
							(d.head || d.body).appendChild(s);
						})();
					</script>
					<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
				</div>
				<div class="row">
					<div class="col-lg-6 col-sm-12">
						<div class="widget-header position-relative mb-10" style="border-bottom: 1px solid #d4cfcf;">
							<div class="row">
								<div class="col-12">
									<h4 class="widget-title mb-0">Postingan <span>terkait</span></h4>
								</div>
							</div>
						</div>
						<div class="loop-list-style-1 background-white border-radius-10 mb-30 wow fadeIn animated">
							<?php 
								$related = Logic::getRelatedArticle($artikel->id, $artikel->slug_judul);
								if(!empty($related)){
									foreach($related as $rdx=>$rrow){
										$backgroundrelated = Logic::gambarPertama($rrow['isi']);
							?>
										<article class="pl-10 pt-10 pr-10">
											<div class="d-flex">
												<div class="post-content media-body">
													<h6 class="post-title text-limit-2-row">
														<a href="<?php echo Url::to(['/content/index', 'judul'=>$rrow['slug_judul']]); ?>"><?php echo $rrow['judul']; ?></a>
													</h6>
													<hr class="wp-block-separator is-style-wide mb-0">
												</div>
											</div>
										</article>
							<?php 
									} 
								} 
							?>
						</div>
					</div>
					<div class="col-lg-6 col-sm-12">
						<div class="widget-header position-relative mb-10" style="border-bottom: 1px solid #d4cfcf;">
							<div class="row">
								<div class="col-12">
									<h4 class="widget-title mb-0">Baca <span>juga</span></h4>
								</div>
							</div>
						</div>
						<div class="loop-list-style-1 background-white border-radius-10 mb-30 wow fadeIn animated">
							<?php 
								$bacajuga = Logic::getBacaJuga($artikel->id);
								if(!empty($bacajuga)){
									foreach($bacajuga as $rdx=>$rrow){
										$backgroundrelated = Logic::gambarPertama($rrow->isi);
							?>
										<article class="pl-10 pt-10 pr-10">
											<div class="d-flex">
												<div class="post-content media-body">
													<h6 class="post-title text-limit-2-row">
														<a href="<?php echo Url::to(['/content/index', 'judul'=>$rrow->slug_judul]); ?>"><?php echo $rrow->judul; ?></a>
													</h6>
													<hr class="wp-block-separator is-style-wide mb-0">
												</div>
											</div>
										</article>
							<?php 
									} 
								} 
							?>
						</div>	
					</div>	
					<div class="col-sm-12">
						<div class="loop-list-style-1 background-white border-radius-10 mb-30 wow fadeIn animated">
							<article class="pl-10 pt-10 pr-10">
								<ins class="adsbygoogle"
									 style="display:block"
									 data-ad-client="ca-pub-6606907698073340"
									 data-ad-slot="8345490771"
									 data-ad-format="horizontal"
									 data-full-width-responsive="true"></ins>
									<script>
										(adsbygoogle = window.adsbygoogle || []).push({});
									</script>
							</article>
						</div>		
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-12 primary-sidebar sticky-sidebar sidebar-right order-2 order-md-2">
				<?php echo $this->render('/site/_arsipberita', ['menu'=>$menu]); ?>
				
				<div class="sidebar-widget mb-30">
					<div class="widget-header position-relative mb-10" style="border-bottom: 1px solid #d4cfcf;">
						<div class="row">
							<div class="col-12">
								<h4 class="widget-title mb-0">Postingan <span>terpopuler</span></h4>
							</div>
						</div>
					</div>
					<div id="loaderpopulerpost"></div>
					<div class="post-aside-style-3 bg-white border-radius-15" id="datapopulerpost"></div>
				</div>	
				
				<div class="widget-header position-relative mb-10" style="border-bottom: 1px solid #d4cfcf;">
					<div class="row">
						<div class="col-12">
							<h4 class="widget-title mb-0"><i class="icofont icofont-ui-video-play"></i> Video <span>Terbaru</span></h4>
						</div>
					</div>
				</div>
				<div id="loaderlatestvideo"></div>
				<div class="post-aside-style-3" id="datalatestvideo"></div>
			</div>	
		</div>
	</div>
</div>
		
<script>
	$(document).ready(function() {
		showcorona('Indonesia', '<?php echo Url::toRoute(['/site/corona']); ?>');
		showlatestvideo('<?php echo $menu->slug_name;?>', '<?php echo Url::toRoute(['/site/latestvideo']); ?>');
		showpopulerpost('<?php echo $menu->slug_name;?>', '<?php echo Url::toRoute(['/site/populerpost']); ?>');
	});
</script>