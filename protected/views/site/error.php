<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>

<title> ERRNO PAGE</title>

<?php /*
<h1 class="error-title font-weight-boldest text-info mt-10 mt-md-0 mb-12">Oops!</h1>
<p class="font-weight-boldest display-4"> <?= nl2br(Html::encode($message)) ?></p>
<p class="font-size-h3">We're working on it and we'll get it fixedas soon possible.You can back or use our Help Center.</p>
<div><a class="btn btn-sm btn-pill btn-info" href="javascript:history.go(-1)">Kembali ke Halaman Sebelumnya</a></div>
*/ ?>

<div class="row mb-30">
	<div class="col-12">
		<div class="content-404 text-center mb-30">
			<h1 class="mb-30">404</h1>
			<p>The page you were looking for could not be found.</p>
			<p class="text-muted">You may have typed the address incorrectly or you may have used an outdated link, or that page is traveling on Mars :) </p>
		</div>
	</div>
</div>