<?php
	use Yii;
	use app\models\TrArtikel;
	use app\models\Menu;
	use app\components\Logic;
	use yii\helpers\Url;
	
	$slider = Logic::hasSlider(10);
?>

<title>Inibabad.com - Portal Berita Terbaru Hari Ini, Jernih Memandang Sekitar, Tempat Kita Jumpa</title>

<span style="display:none"><h1>INIBABAD - JERNIH MEMANDANG SEKITAR</h1></span>

<div class="row mb-15">
	<div class="col-md-12">
		<div class="widget-header position-relative mb-30">
			<span class="widget-title mb-0"></span>
		</div>
		<div class="post-carausel-1 post-module-1 row">
			<?php foreach($slider['hasil'] as $ddx=>$drow){ ?>
				<div class="col">
					<div class="slider-single bg-white p-10 border-radius-15">
						<div class="img-hover-scale border-radius-10">
							<a href="<?php echo Url::to(['/content/index', 'judul'=>$drow['slug_judul']]); ?>">
								<div class="thumb-overlay img-hover-slide-depan border-radius-15" style="background-image: url(<?php echo $drow['gambar']; ?>)"></div>
								<div style="display:none">Background Slider - <?php echo $drow['menu']; ?> - <?php echo substr($drow['judul'],0,50); ?></div>
							</a>
						</div>
						
						<div class="entry-meta meta-0 font-small mt-15 mb-15">
							<a href="<?php echo Url::to(['/list/index', 'name'=>$drow['slug_name']]); ?>"><span class="post-cat bg-danger color-white"><?php echo $drow['menu_icon']; ?> <span style="display:none">Judul Slider - <?php echo $drow['menu']; ?> - <?php echo substr($drow['judul'],0,50); ?></span><?php echo $drow['menu']; ?></span></a>
						</div>
						<h6 class="post-title text-limit-2-row">
							<a href="<?php echo Url::to(['/content/index', 'judul'=>$drow['slug_judul']]); ?>"><?php echo $drow['judul']; ?></a>
						</h6>
						<div class="entry-meta meta-1 font-x-small mt-10 pr-5 pl-5 text-muted">
							<div>
								<span class="mr-5 text-muted"><i class="icofont icofont-calendar"></i></span><?php echo $drow['created_date']; ?>
								<span class="mr-5"><i class="icofont icofont-eye"></i></span><?php echo $drow['total_views']; ?>
							</div>
						</div>
							
						
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 primary-sidebar sticky-sidebar sidebar-left order-1 order-md-1">
		<div class="latest-post mb-15">
			<div class="widget-header position-relative mb-10" style="border-bottom: 1px solid #d4cfcf;">
				<div class="row">
					<div class="col-12">
						<h4 class="widget-title mb-0"><i class="icofont icofont-dna-alt-1"></i> Perkembangan Virus <span>Corona</span></h4>
					</div>
				</div>
			</div>
			<div id="loadercorona"></div>
			<div class="post-aside-style-3 bg-white border-radius-15" id="datacorona"></div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-3 col-md-3 primary-sidebar sticky-sidebar sidebar-left order-2 order-md-1">
		<?php echo $this->render('_arsipberita', ['menu'=>$menu]); ?>
		
		<div class="latest-post mb-50">
			<div class="widget-header position-relative mb-10" style="border-bottom: 1px solid #d4cfcf;">
				<div class="row">
					<div class="col-12">
						<h4 class="widget-title mb-0"><i class="icofont icofont-ui-video-play"></i> Video <span>Terbaru</span></h4>
					</div>
				</div>
			</div>
			<div id="loaderlatestvideo"></div>
			<div class="post-aside-style-3" id="datalatestvideo"></div>
		</div>
	</div>
	<div class="col-lg-9 col-md-9 order-1 order-md-2">
		<div class="row">
			<div class="col-lg-8 col-md-12">
				<?php echo $this->render('_latestpost'); ?>
			</div>
			<div class="col-lg-4 col-md-12 primary-sidebar sticky-sidebar sidebar-right order-2 order-md-1">
				<div class="sidebar-widget mb-30">
					<div class="widget-header position-relative mb-10" style="border-bottom: 1px solid #d4cfcf;">
						<div class="row">
							<div class="col-12">
								<h4 class="widget-title mb-0">Postingan <span>terpopuler</span></h4>
							</div>
						</div>
					</div>
					<div id="loaderpopulerpost"></div>
					<div class="post-aside-style-3 bg-white border-radius-15" id="datapopulerpost"></div>
				</div>	
			</div>	
		</div>
	</div>	
</div>

<script>
	$(document).ready(function() {
		showcorona('Indonesia', '<?php echo Url::toRoute(['/site/corona']); ?>');
		showlatestvideo(null, '<?php echo Url::toRoute(['/site/latestvideo']); ?>');
		showpopulerpost(null, '<?php echo Url::toRoute(['/site/populerpost']); ?>');
	});
</script>