<style>
	#datacorona table{
		white-space: nowrap;
	}
</style>

<article class="card card-body">
	<div class="table-responsive">
		<table class="table table-sm table-striped" id="datacorona">
			<thead>
				<tr>
					<th>Negara</th>
					<th>Kasus Positif</th>
					<th>Meninggal Dunia</th>
					<th>Sembuh</th>
					<th>Dirawat</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?php echo $apisingle['country']; ?></td>
					<td>
						<div>Total : <?php echo NUMBER_FORMAT($apisingle['cases']); ?></div>
						<div>Hari ini : <?php echo NUMBER_FORMAT($apisingle['todayCases']); ?></div>
					</td>
					<td>
						<div>Total : <?php echo NUMBER_FORMAT($apisingle['deaths']); ?></div>
						<div>Hari ini : <?php echo NUMBER_FORMAT($apisingle['todayDeaths']); ?></div>
					</td>
					<td>
						<div>Total : <?php echo NUMBER_FORMAT($apisingle['recovered']); ?></div>
						<div>Hari ini : <?php echo NUMBER_FORMAT($apisingle['todayRecovered']); ?></div>
					</td>
					<td>Total : <?php echo NUMBER_FORMAT($apisingle['active']); ?></td>
				</tr>
				<tr>
					<td>Seluruh Dunia</td>
					<td>
						<div>Total : <?php echo NUMBER_FORMAT($dataall['cases']); ?></div>
						<div>Hari ini : <?php echo NUMBER_FORMAT($dataall['todayCases']); ?></div>
					</td>
					<td>
						<div>Total : <?php echo NUMBER_FORMAT($dataall['deaths']); ?></div>
						<div>Hari ini : <?php echo NUMBER_FORMAT($dataall['todayDeaths']); ?></div>
					</td>
					<td>
						<div>Total : <?php echo NUMBER_FORMAT($dataall['recovered']); ?></div>
						<div>Hari ini : <?php echo NUMBER_FORMAT($dataall['todayRecovered']); ?></div>
					</td>
					<td>Total : <?php echo NUMBER_FORMAT($dataall['active']); ?></td>
				</tr>
			<tbody>
		</table>
	</div>
	
	<div class="text-center">
		For information about COVID-19, visit <b><a href="http://covid19.go.id/" target="blank">covid19.go.id</a></b>
	</div>
</article>