<?php
	use Yii;
	use app\models\TrArtikel;
	use app\models\Menu;
	use app\components\Logic;
	use yii\helpers\Url;

	if(!empty($menu)){
		$artikellock = TrArtikel::find()->select('GROUP_CONCAT(id) dataid')->andWhere('created_date BETWEEN DATE_SUB(NOW(), INTERVAL 60 DAY) AND NOW()')->andWhere('menu_id = :param1', [':param1'=>$menu->id])->orderBy(['created_date'=>SORT_DESC])->one(); 
	}else{
		$artikellock = TrArtikel::find()->select('GROUP_CONCAT(id) dataid')->andWhere('created_date BETWEEN DATE_SUB(NOW(), INTERVAL 60 DAY) AND NOW()')->orderBy(['created_date'=>SORT_DESC])->one(); 
	}
	$artikelpopuler = TrArtikel::find()->andWhere('id IN ('.$artikellock->dataid.')')->orderBy(['total_views'=>SORT_DESC])->limit(5)->all(); 
	if(!empty($artikelpopuler)){
		$rank = 0;
		foreach($artikelpopuler as $pdx=>$prow){
			$rank += 1;
			$backgroundpopuler = Logic::gambarPertama($prow->isi);
?>
			<article class="pl-10 pt-10 pr-10" style="visibility: visible; animation-name: fadeIn;">
				<div class="pl-10 pr-10">
					<div class="entry-meta mb-15">
						<a class="entry-meta meta-0" href="<?php echo Url::to(['/list/index', 'name'=>$prow->menu->name]); ?>"><span class="post-in post-in btn btn-outline-danger font-x-small font-x-small"><span style="display:none;">Menu - Populer - <?php echo substr($prow->judul,0,50); ?> </span> <?php echo $prow->menu->name; ?></span></a>
					</div>
					<h6 class="post-title text-limit-2-row mb-15">#<?php echo $rank; ?> <i class="icofont icofont-trophy text-warning"></i> <a href="<?php echo Url::to(['/content/index', 'judul'=>$prow->slug_judul]); ?>"><span style="display:none;">Judul - Populer - <?php echo substr($prow->judul,0,50); ?> </span><?php echo $prow->judul; ?></a></h6>
					<div class="entry-meta meta-1 font-x-small color-grey text-uppercase mb-10">
						<div class="mb-10">
							<span class="post-on"><i class="icofont icofont-calendar"></i> <?php echo $prow->created_date; ?></span>
							<span class="post-on"><i class="icofont icofont-eye"></i> <?php echo NUMBER_FORMAT($prow->total_views); ?></span>
						</div>
					</div>
					<hr class="wp-block-separator is-style-wide mb-0">
				</div>
			</article>
<?php 
		}
	}else{ 
?>	
		<article class="bg-white border-radius-15 mb-30 p-10 wow fadeIn  animated" style="visibility: visible; animation-name: fadeIn;">
			<div class="pl-10 pr-10">
				<h6 class="post-title mb-15"><a href="single.html">Postingan terpopuler tidak ditemukan</a></h6>
			</div>
		</article>
<?php } ?>