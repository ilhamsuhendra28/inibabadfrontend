<?php
	use Yii;
	use app\models\TrArtikel;
	use app\models\Menu;
	use app\components\Logic;
	use app\components\Api;
	use yii\helpers\Url;
	// var_dump($menu->id);exit;
	
	if(!empty($menu)){
		$artikelvideo = TrArtikel::find()->andWhere('menu_id = :param1 AND isi REGEXP \'iframe\' = :param2', [':param1'=>$menu->id, ':param2'=>1])->limit(5)->orderBy(['created_date'=>SORT_DESC])->all();
	}else{
		$artikelvideo = TrArtikel::find()->andWhere('isi REGEXP \'iframe\' = :param1', [':param1'=>1])->limit(5)->orderBy(['created_date'=>SORT_DESC])->all();		
	}
	$video = [];
	foreach($artikelvideo as $vdx=>$vrow){
		if(Logic::videoPertama($vrow->isi) != ''){
			$video[] = Logic::videoPertama($vrow->isi);
			$artjudul[] = $vrow->slug_judul;
		}
	}
	$listvideo = $video;

	if(!empty($listvideo)){
		foreach($listvideo as $vdx=>$vrow){ 
		$expvideo = explode('/',$vrow);
		$descvideo = Api::videoyoutube(end($expvideo));
		$bg = $descvideo['items'][0]['snippet']['thumbnails']['high']['url'];
		$title = $descvideo['items'][0]['snippet']['title'];
		$created_by = $descvideo['items'][0]['snippet']['channelTitle'];
		$created_date = $descvideo['items'][0]['snippet']['publishedAt'];
		$views = $descvideo['items'][0]['statistics']['viewCount'];
		$loves = $descvideo['items'][0]['statistics']['likeCount'];
		// echo '<pre>';
		// var_dump($descvideo['items'][0]);
		if(!empty($title)){
?>
			<article class="bg-white border-radius-15 mb-30 p-10">
				<div class="img-hover-slide border-radius-15 mb-10 position-relative overflow-hidden" style="min-height: 150px !important;">
					<a onclick="loadvideo('<?php echo $title; ?>', '<?php echo $vrow; ?>')" href="javascript:void(0)" data-toggle="modal" data-target="#loadmodal" data-keyboard="false" data-backdrop="static">
						<div class="thumb-overlay img-hover-slide border-radius-15 position-relative" style="background-image: url(<?php echo $bg; ?>); background-position:center !important;min-height:150px !important;"></div>
						<div style="display:none">Video Background - <?php echo $title; ?></div>
					</a>
				</div>
				<div class="pl-10 pr-10">
					<h6 class="post-title mb-15"><a href="<?php echo Url::to(['/content/index', 'judul'=>rawurlencode($artjudul[$vdx])]); ?>"><span style="display:none;">Video - </span><?php echo $title; ?></a></h6>
					<div class="entry-meta meta-1 font-x-small color-grey float-left text-uppercase mb-10">
						<div class="mb-10">
							<span class="post-by"><i class="icofont icofont-user"></i> Oleh <?php echo $created_by; ?></span>
							<span class="post-by"><i class="icofont icofont-eye"></i><?php echo NUMBER_FORMAT($views); ?></span>
							<span class="post-by"><i class="icofont icofont-love"></i><?php echo NUMBER_FORMAT($loves); ?></span>
						</div>
						<div>
							<span class="post-by"><i class="icofont icofont-calendar"></i> Diposting <?php echo Logic::convertDate($created_date); ?></span>
						</div>
					</div>
				</div>
			</article>
			
			<script>
				function loadvideo(judul, url){
					$('#loadmodal').find('.modal-header .modal-title').html('<i class="icofont icofont-ui-video-chat"></i> '+judul);
					
					$.ajax({
						url: '<?php echo Url::toRoute(['site/loadvideo']); ?>',
						type: 'POST',
						dataType: "html",
						data:{
							dataurl:url
						},
						beforeSend: function() {
							$("#loadermodal").html('<div class="loader-box"><div class="loader-30"></div></div>');
							$("#datamodal").html('');
						},
						success: function(data){
							$("#datamodal").html(data);
						},
						complete: function(){
							$("#datamodal").show();
							$("#loadermodal").html('');
						},
						error: function (xhr, ajaxOptions, thrownError) {
							var pesan = xhr.status + " " + thrownError + "\n" +  xhr.responseText;
							$("#datamodal").html('<div class="alert alert-danger inverse alert-dismissible fade show m-0" role="alert"><i class="icofont icofont-warning-alt"></i> '+pesan+'</div>');
						},
					});
				}
			</script>
<?php 
			}
		}
	}else{
?>
		<article class="bg-white border-radius-15 mb-30 p-10 wow fadeIn  animated" style="visibility: visible; animation-name: fadeIn;">
			<div class="pl-10 pr-10">
				<h6 class="post-title mb-15"><a href="single.html">Video terbaru tidak ditemukan</a></h6>
			</div>
		</article>
<?php 
	} 
?>
	