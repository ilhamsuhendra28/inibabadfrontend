<?php
	use Yii;
	use yii\helpers\Url;
	use yii\helpers\ArrayHelper;
	use yii\helpers\Html;
	use app\components\Logic;
	use yii\bootstrap\ActiveForm;
?>

<div class="latest-post mb-50">
	<div class="widget-header mb-10">
		<div class="row">
			<div class="col-12">
				<h4 class="widget-title mb-0"><i class="icofont icofont-newspaper"></i> Arsip <span>Berita</span></h4>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-body" style="border-radius:15px;">
			<div class="form-grup">
				<?= Html::dropDownList('tahun', null, ArrayHelper::map(Logic::getTahun(), 'tahun', 'tahun'), ['onchange'=>'showbulan($(this).val())']) ?>
			</div>
			
			<div id="loaderarsipbulan"></div>
			<div id="dataarsipbulan">
				
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		showbulan(<?php echo date('Y'); ?>);
	});
	
	function showbulan(tahun){
		$.ajax({
			type: "POST",
			url: '<?php echo Url::toRoute(['/arsip/databulan']); ?>',
			dataType: "html",
			data:{
				tahun:tahun
			},
			beforeSend: function() {
				$("#loaderarsipbulan").html('<div class="loader-box"><div class="loader-30"></div></div>');
				$("#dataarsipbulan").html('');
			},
			success: function(data){
				$("#dataarsipbulan").html(data);
			},
			complete: function(){
				$("#loaderarsipbulan").html('');				
			},
			error: function (xhr, ajaxOptions, thrownError) {
				var pesan = xhr.status + " " + thrownError + "\n" +  xhr.responseText;
				$("#dataarsipbulan").html('<div class="alert alert-danger dark" role="alert"><i class="icofont icofont-warning-alt"></i> '+pesan+'</div>');
			},
		});
	}
</script>