<?php
	use Yii;
	use app\models\TrArtikel;
	use app\models\Menu;
	use app\components\Logic;
	use yii\helpers\Url;
?>

<div class="latest-post mb-50">
	<?php
		$menukategori = Menu::find()->andWhere('is_frontend = :param1', [':param1'=>1])->orderBy(['order'=>SORT_ASC])->all();
		foreach($menukategori as $mdx=>$mrow){
			$artikelone = TrArtikel::find()->andWhere('menu_id = :param1', [':param1'=>$mrow->id])->orderBy(['id'=>SORT_DESC])->one();
			if(!empty($artikelone)){
				$backgroundone = Logic::gambarPertama($artikelone->isi);
				if($mdx != 0){
	?>
					<div class="loop-list-style-1">
						<article class="first-post p-10 background-white border-radius-10 mb-30 wow fadeIn animated">
							<div class="pr-10 pl-10">
								<div class="entry-meta mb-10">
									<ins class="adsbygoogle"
									 style="display:block"
									 data-ad-client="ca-pub-6606907698073340"
									 data-ad-slot="8345490771"
									 data-ad-format="horizontal"
									 data-full-width-responsive="true"></ins>
									<script>
										(adsbygoogle = window.adsbygoogle || []).push({});
									</script>
								</div>
							</div>
						</article>
					</div>
	<?php 
				} 
	?>
				<div class="widget-header position-relative mb-10" style="border-bottom: 1px solid #d4cfcf;">
					<div class="row">
						<div class="col-9">
							<h4 class="widget-title mb-0"><?php echo $mrow->data; ?> <?php echo $mrow->name; ?></h4>
						</div>
					</div>
				</div>
				<div class="loop-list-style-1">
					<article class="first-post p-10 background-white border-radius-10 mb-30 wow fadeIn animated">
						<div class="img-hover-slide border-radius-15 mb-10 position-relative overflow-hidden">
							<span class="top-right-icon bg-dark"><i class="mdi mdi-flash-on"></i></span>
							<a href="<?php echo Url::to(['/content/index', 'judul'=>$artikelone->slug_judul]); ?>">
								<div class="thumb-overlay img-hover-slide border-radius-15 position-relative" style="background-image: url(<?php echo $backgroundone; ?>)"></div>
								<div style="display:none"><?php echo $mrow->name; ?> - <?php echo $artikelone->judul; ?></div>
							</a>
						</div>
						<div class="pr-10 pl-10">
							<div class="entry-meta mb-10">
								<a class="entry-meta meta-0" href="<?php echo Url::to(['/list/index', 'name'=>$mrow->name]); ?>"><span class="post-in btn btn-outline-danger font-x-small"><span style="display:none;">Menu Editor - </span><?php echo $mrow->name; ?></span></a>
								<div class="float-right font-small">
									<span class="ml-30"><a href="<?php echo Url::to(['/content/index', 'judul'=>$artikelone->slug_judul]); ?>" class="read-more"><span class="mr-10"><i class="fa fa-thumbtack" aria-hidden="true"></i></span> <span style="display:none;"><?php echo $mrow->name; ?> - </span> Dipilih oleh editor</a></span>
								</div>
							</div>
							<h4 class="post-title mb-20">
								<span class="post-format-icon">
									<i class="icofont icofont-paper"></i>
								</span>
								<a href="<?php echo Url::to(['/content/index', 'judul'=>$artikelone->slug_judul]); ?>"><span style="display:none;">Judul Editor - </span><?php echo $artikelone->judul; ?></a>
							</h4>
							<div class="mb-20 overflow-hidden">
								<div class="entry-meta meta-1 font-x-small color-grey text-uppercase">
									<span class="post-by"><i class="icofont icofont-calendar"></i> <?php echo $artikelone->created_date; ?></span>
									<span class="post-by"><i class="icofont icofont-eye"></i><?php echo NUMBER_FORMAT($artikelone->total_views); ?></span>
									<span class="post-by"><i class="icofont icofont-comment"></i><a href="<?php echo Url::to(['/content/index', 'judul'=>$artikelone->slug_judul]); ?>#disqus_thread">0 KOMENTAR</a></span>
								</div>
							</div>
							<hr class="wp-block-separator is-style-wide">
							<div class="entry-main-content">
								<p class="text-justify"><?php echo substr(Logic::removeNbsp(strip_tags($artikelone->isi)), 0,150); ?>...</p>
							</div>
						</div>
						
						<div class="text-right">
							<h6 class="font-medium">
								<a class="btn btn-sm btn-pill btn-info" href="<?php echo Url::to(['/content/index', 'judul'=>$artikelone->slug_judul]); ?>"><i class="icofont icofont-eye"></i> Baca selengkapnya <span style="display:none;"><?php echo $mrow->name; ?> - <?php echo substr($artikelone->judul,0,50); ?></span></a>
							</h6>
						</div>
						
					</article>
					<?php
						$artikeltwo = TrArtikel::find()->andWhere('menu_id = :param1 AND id != :param2', [':param1'=>$mrow->id, ':param2'=>$artikelone->id])->orderBy(['id'=>SORT_DESC])->limit(2)->all();
						if(!empty($artikeltwo)){
							foreach($artikeltwo as $arow){
								$backgroundtwo = Logic::gambarPertama($arow->isi);
					?>
								<article class="p-10 background-white border-radius-10 mb-30 wow fadeIn animated">
									<div class="d-flex">
										
										<div class="post-content media-body">
											<div class="entry-meta mb-15">
												<a class="entry-meta meta-0" href="<?php echo Url::to(['/list/index', 'name'=>$mrow->name]); ?>"><span class="post-in post-in btn btn-outline-danger font-x-small font-x-small"><span style="display:none;">Menu Non Editor - <?php echo substr($arow->judul,0,50); ?> - </span><?php echo $mrow->name; ?></span></a> <a class="vertical-align: middle;" href="<?php echo Url::to(['/content/index', 'judul'=>$arow->slug_judul]); ?>"><span style="display:none;">Judul Non Editor - </span><?php echo $arow->judul; ?></a>
											</div>
											<div class="entry-meta meta-1 font-x-small color-grey text-uppercase">
												<div class="mb-10">
													<span class="post-by"><i class="icofont icofont-calendar"></i> <?php echo $arow->created_date; ?></span>
													<span class="post-by"><i class="icofont icofont-eye"></i><?php echo NUMBER_FORMAT($arow->total_views); ?></span>
													<span class="post-by"><i class="icofont icofont-comment"></i><a href="<?php echo Url::to(['/content/index', 'judul'=>$arow->slug_judul]); ?>#disqus_thread">0 KOMENTAR</a></span>
												</div>
											</div>
											<hr class="wp-block-separator is-style-wide">
											<div class="entry-main-content">
												<p class="text-justify"><?php echo substr(Logic::removeNbsp(strip_tags($arow->isi)), 0,150); ?>...</p>
											</div>
											<div class="float-right mt-10">
												<h6 class="font-medium">
													<a class="btn btn-sm btn-pill btn-info" href="<?php echo Url::to(['/content/index', 'judul'=>$arow->slug_judul]); ?>"><i class="icofont icofont-eye"></i> Baca selengkapnya<span style="display:none;"> <?php echo $mrow->name; ?> - <?php echo substr($arow->judul,0,50); ?></span></a>
												</h6>
											</div>
										</div>
									</div>
								</article>
					<?php 
							}
						} 
					?>
				</div>
	<?php 
			} 
		}
	?>
</div>