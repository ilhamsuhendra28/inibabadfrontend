<?php
	use Yii;
	use yii\helpers\Url;
?>

<?php 
	if(!empty($bulan)){
		foreach($bulan as $bdx=>$brow){
?>
			<a href="<?php echo Url::to(['arsip/index', 'tahun'=>$brow['tahun'], 'bulan'=>$brow['bulan']]);?>"><div class="badge badge-pill badge-dark"><?php echo $brow['bulan_format'].' ('.$brow['jumlah'].')';?></div></a>
<?php
		}
	}
?>