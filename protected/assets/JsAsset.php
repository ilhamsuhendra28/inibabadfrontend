<?php
namespace app\assets;

use yii\web\AssetBundle;

class JsAsset extends AssetBundle
{
	// public $basePath = '@webroot';
    public $sourcePath  = '@webroot/themes/customilham/assets/js';
    public $css = [];
    public $js = [
		[	
			'source' => 'vendor/jquery.min.js',
			'position' => \yii\web\View::POS_HEAD
		],
		'vendor/bundle.min.js'
    ];
    public $depends = [];
}
