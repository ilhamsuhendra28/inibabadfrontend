<?php
namespace app\assets;

use yii\web\AssetBundle;

class CssAsset extends AssetBundle
{
	// public $basePath = '@webroot';
    public $sourcePath  = '@webroot/themes/customilham/assets/css';
    public $css = [
        'bundle.min.css',
    ];
    public $js = [];
    public $depends = [];
}
