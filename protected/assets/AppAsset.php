<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [];
    public $js = [];
    public $depends = [
		'\app\assets\CssAsset',
		'\app\assets\JsAsset',
		// 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',		
	];
}
