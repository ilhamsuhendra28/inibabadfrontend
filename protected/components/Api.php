<?php	
	namespace app\components;
	use Yii;
	use yii\helpers\Html;
	
	Class Api {
		function curlget($url){
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			$postResult = curl_exec($ch);

			curl_close($ch);
			$postresponse = json_decode($postResult, true);
			if($postresponse == NULL){
				$response['data']['message'] = $postResult;
			}else{
				$response = $postresponse;
			}
			
			return $response;
		}
		
		function curlpost($url, $payload, $token){
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $url); 
			curl_setopt($ch, CURLOPT_POST, 1); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $payload); 
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			
			$postToken = curl_exec($ch); 
			curl_close($ch);
			$postresponse = json_decode($postToken, true);
			if($postresponse == NULL){
				$response['data']['message'] = $postToken;
			}else{
				$response = $postresponse;
			}	
			
			return $response;							
		}
		
		function listslider($limit){
			$url = Yii::$app->params['host']."/api/default/slider?limit=".$limit;
			$response = Api::curlget($url);
			
			return $response;
		}
		
		function videoyoutube($id){
			$url = 'https://www.googleapis.com/youtube/v3/videos?id='.$id.'&key=AIzaSyB4FmQD8urdMhU_759Lo5QtaqqK8iCTcTc&part=snippet,statistics';
			$response = Api::curlget($url);
			
			return $response;
		}
		
		function datacoronasingle($negara){
			$url = 'https://disease.sh/v3/covid-19/countries/'.$negara.'?yesterday=true&twoDaysAgo=true&strict=true';
			$response = Api::curlget($url);
			
			return $response;
		}
		
		function datacoronaall(){
			$url = 'https://disease.sh/v3/covid-19/countries?yesterday=true&twoDaysAgo=true';
			$response = Api::curlget($url);
			
			return $response;
		}
    }
?>
