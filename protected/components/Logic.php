<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components;

use yii;
use yii\helpers\Url;
use app\models\Menu;
use app\models\TrArtikel;
use DateTime;

class Logic
{
    public function hasMenu()
    {
		$data = [];
		$datachild = [];
        $menuchild = Menu::find()->andWhere('is_frontend = :param1 AND parent IS NOT NULL', [':param1'=>1])->orderBy(['order'=>SORT_ASC])->all();
		foreach($menuchild as $cdx=>$crow){
			$datachild[$crow->parent][$crow->id]['id'] = $crow->id;
			$datachild[$crow->parent][$crow->id]['parent'] = $crow->parent;
			$datachild[$crow->parent][$crow->id]['name'] = $crow->name;
			$datachild[$crow->parent][$crow->id]['slug_name'] = $crow->slug_name;
			$datachild[$crow->parent][$crow->id]['data'] = $crow->data;
			$datachild[$crow->parent][$crow->id]['route'] = $crow->route;
			$datachild[$crow->parent][$crow->id]['order'] = $crow->order;
		}
		
        $menuparent = Menu::find()->andWhere('is_frontend = :param1 AND parent IS NULL', [':param1'=>1])->orderBy(['order'=>SORT_ASC])->all();
		if(!empty($menuparent)){
			foreach($menuparent as $mdx=>$mrow){
				$data[$mrow->id]['id'] = $mrow->id;
				$data[$mrow->id]['name'] = $mrow->name;
				$data[$mrow->id]['slug_name'] = $mrow->slug_name;
				$data[$mrow->id]['data'] = $mrow->data;
				$data[$mrow->id]['route'] = $mrow->route;
				$data[$mrow->id]['order'] = $mrow->order;
				if($datachild[$mrow->id] != ''){
					$data[$mrow->id]['child'] = $datachild[$mrow->id];
				}else{
					$data[$mrow->id]['child'] = [];
				}
			}
		}
		
		return $data;
    }
	
	public static function convertDate($date)
    {
        $dt = new DateTime($date);
        return $date ? $dt->format('d-m-Y') : null;
    }
	
    public static function dbFormatDate($date)
    {
        $dt = new DateTime($date);
        return $date ? $dt->format('Y-m-d') : null;
    }
	
    function Timeago($datetime, $full = false)
    {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'tahun',
            'm' => 'bulan',
            'w' => 'minggu',
            'd' => 'hari',
            'h' => 'jam',
            'i' => 'menit',
            's' => 'detik',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
            } else {
                unset($string[$k]);
            }
        }
        $string = array_slice($string, 0, ($full) ? 2 : 1);

        return $string ? implode(', ', $string) . ' yang lalu' : 'baru saja';
    }

    public function hasRole($role_name, $user_id)
    {
        $authManager = \Yii::$app->getAuthManager();
        return $authManager->getAssignment($role_name, $user_id) ? true : false;
    }
	
	public function gambarPertama($string, $default = NULL) {
        preg_match('@<img.+src="(.*)".*>@Uims', $string, $matches);
       
        $src = $matches ? $matches[1] : $default;
        return htmlentities($src);
    }
	
	public function gambarAll($string, $default = NULL) {
        preg_match_all('@<img.+src="(.*)".*>@Uims', $string, $matches);
       
        $src = $matches ? $matches[1] : $default;
        return $src;
    }
	
	public function videoPertama($string, $default = NULL) {
        preg_match('/<iframe.*src=\"(.*)\".*><\/iframe>/isU', $string, $matches);
       
        $src = $matches ? $matches[1] : $default;
        return $src;
    }
	
	public function removeNbsp($value){
		$find = ['/^((?:&nbsp;|\s)+)|(?1)$/', '/\s*&nbsp;(?:\s*&nbsp;)*\s*/', '/\s+/'];
		$replace = ['', '&nbsp;',' '];
		return preg_replace($find, $replace, trim($value));
	}
	
	public function hasSlider($limit){
		$model = TrArtikel::find()->limit($limit)->orderBy(['id'=>SORT_DESC])->all();
		if(!empty($model)){
			$data['code'] = 200;
			$data['status'] = 'Success';
			$data['message'] = 'Slider telah ditemukan';
			foreach($model as $mdx=>$mrow){
				$data['hasil'][$mdx]['id'] = $mrow->id;
				$data['hasil'][$mdx]['menu'] = $mrow->menu->name;
				$data['hasil'][$mdx]['slug_name'] = $mrow->menu->slug_name;
				$data['hasil'][$mdx]['menu_icon'] = $mrow->menu->data;
				$data['hasil'][$mdx]['judul'] = $mrow->judul;
				$data['hasil'][$mdx]['slug_judul'] = $mrow->slug_judul;
				$data['hasil'][$mdx]['sub_isi'] = $mrow->sub_isi;
				$data['hasil'][$mdx]['total_views'] = $mrow->total_views;
				$data['hasil'][$mdx]['created_date'] = $mrow->created_date;
				$data['hasil'][$mdx]['time_ago'] = Logic::Timeago($mrow->created_date);
				$data['hasil'][$mdx]['created_by'] = $mrow->createdBy->username;
				$data['hasil'][$mdx]['gambar'] = Logic::gambarPertama($mrow->isi);
				$data['hasil'][$mdx]['video'] = Logic::videoPertama($mrow->isi);
			}
		}else{
			$data['code'] = 200;
			$data['status'] = 'Failed';
			$data['message'] = 'Slider tidak ditemukan';
			$data['hasil'] = [];
		}
		return $data;
	}
	
	public static function getRelatedArticle($id_artikel, $isiartikel){
		$db = Yii::$app->db;
        $sqldata = $db->createCommand('SELECT a.*, a.slug_judul, b.name, MATCH(a.judul, a.isi, a.sub_isi) AGAINST (\''.(strtolower($isiartikel)).'\' IN NATURAL LANGUAGE MODE) as score
		FROM tr_artikel a
		JOIN menu b ON b.id = a.menu_id
		WHERE a.id <> :param1
		ORDER BY score DESC
		LIMIT 5');
		$sqldata->bindParam(':param1', $id_artikel);
		
		return $sqldata->queryAll();
	}
	
	public static function getBacaJuga($id_artikel){
		$artikel = TrArtikel::find()->andWhere('id <> :param1', [':param1'=>$id_artikel])->orderBy(['rand()'=>SORT_ASC])->limit(5)->all();
		
		return $artikel;
	}
	
	public static function getTahun(){
		$db = Yii::$app->db;
        $sqldata = $db->createCommand('SELECT YEAR(created_date) tahun
		FROM tr_artikel
		GROUP BY tahun DESC');
		
		return $sqldata->queryAll();
	}
	
	public static function getBulan($tahun){
		$selecttahun = strip_tags($tahun);
		
		$db = Yii::$app->db;
        $sqlexec = $db->createCommand('SET lc_time_names = \'id_ID\'');
		$sqlexec->execute();
		
        $sqldata = $db->createCommand('SELECT '.$selecttahun.' tahun, DATE_FORMAT(created_date,\'%M\') bulan_format, DATE_FORMAT(created_date,\'%m\') bulan, COUNT(id) jumlah
		FROM tr_artikel
		WHERE YEAR(created_date) = :param1
		GROUP BY DATE_FORMAT(created_date,\'%m\')
		ORDER BY bulan DESC');
		$sqldata->bindParam(':param1', $tahun);
		
		return $sqldata->queryAll();
	}
	
	public static function getIndoMonth($month)
    {
        if ($month == 1) $bulan = 'Januari';
        else if ($month == 2) $bulan = 'Februari';
        else if ($month == 3) $bulan = 'Maret';
        else if ($month == 4) $bulan = 'April';
        else if ($month == 5) $bulan = 'Mei';
        else if ($month == 6) $bulan = 'Juni';
        else if ($month == 7) $bulan = 'Juli';
        else if ($month == 8) $bulan = 'Agustus';
        else if ($month == 9) $bulan = 'September';
        else if ($month == 10) $bulan = 'Oktober';
        else if ($month == 11) $bulan = 'November';
        else if ($month == 12) $bulan = 'Desember';

        return $bulan;
    }
}
