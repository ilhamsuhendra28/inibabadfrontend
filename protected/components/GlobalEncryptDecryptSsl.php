<?php
namespace app\components;
 
use yii;
use yii\web\UrlRuleInterface;
use yii\helpers\Url;

class GlobalEncryptDecryptSsl implements UrlRuleInterface{

    var $skey = "ASHGARD123456789"; 

    public function createUrl($manager, $route, $params) {
        $paramString = [];
        foreach($params as $key => $value) {
            if(is_array($value)) {
                foreach($value as $key2 => $value2){
                    if($value2 != NULL){
                        $paramString[] = $key2;
                        $paramString[] = $value2;
                    }
                }
            }else{
                if($value != NULL){
                    $paramString[] = $key;
                    $paramString[] = $value;
                }
            }
        }
		
        $urlString = implode(",", $paramString);
        $paramStringEncoded = $urlString ? $this->encode($urlString) : '';
		if(empty($params)){
			return $route.$paramStringEncoded;			
		}else{
			return $route.'?'.$paramStringEncoded;			
		}
    }

    public function parseRequest($manager, $request) {
		$pathInfo = $request->getPathInfo();
		$getPathUrl = $request->getUrl();
		
		if(empty($pathInfo)){
			$pathInfo = Yii::$app->defaultRoute;
			$getPathUrl = $request->getUrl().Yii::$app->defaultRoute;
		}	
		
		$pathUrl = str_replace('??','?',$getPathUrl);
		$pathParams1 = explode("/", $pathUrl);
		unset($pathParams1[0],$pathParams1[1]);
		
		$pathParams2 = explode("?",end($pathParams1));
		$pathParams3 = end($pathParams2);
		$expParams3 = explode('&_csrf', $pathParams3);
		$pathParams4 = [$expParams3[0]];
		
		$var1 = key($pathParams1);
		unset($pathParams1[$var1]);
		
		$pathParams = array_merge($pathParams1, $pathParams4);
		$count = count($pathParams);
		if($count == 2){
			if (isset($pathParams[1])) {
				$paramStringDecoded = $this->decode($pathParams[1]);
				$params = explode(",", $paramStringDecoded);
				for ($i = 0; $i < count($params); $i+= 2) {
					if (count($params) > ($i + 1)) {
						$_GET[$params[$i]] = $params[$i + 1];
						$_REQUEST[$params[$i]] = $params[$i + 1];
					} else {
						$_GET[$params[$i]] = $params[$i];
						$_REQUEST[$params[$i]] = $params[$i];
					}
				}
			}
		}else if($count == 3){
			if (isset($pathParams[2])) {
				$paramStringDecoded = $this->decode($pathParams[2]);
				$params = explode(",", $paramStringDecoded);
				for ($i = 0; $i < count($params); $i+= 2) {
					if (count($params) > ($i + 1)) {
						$_GET[$params[$i]] = $params[$i + 1];
						$_REQUEST[$params[$i]] = $params[$i + 1];
					} else {
						$_GET[$params[$i]] = $params[$i];
						$_REQUEST[$params[$i]] = $params[$i];
					}
				}
			}
		}else if($count == 4){
			if (isset($pathParams[3])) {
				$paramStringDecoded = $this->decode($pathParams[3]);
				$params = explode(",", $paramStringDecoded);
				for ($i = 0; $i < count($params); $i+= 2) {
					if (count($params) > ($i + 1)) {
						$_GET[$params[$i]] = $params[$i + 1];
						$_REQUEST[$params[$i]] = $params[$i + 1];
					} else {
						$_GET[$params[$i]] = $params[$i];
						$_REQUEST[$params[$i]] = $params[$i];
					}
				}
			}
		}else{
			if (isset($pathParams[4])) {
				$paramStringDecoded = $this->decode($pathParams[4]);
				$params = explode(",", $paramStringDecoded);
				for ($i = 0; $i < count($params); $i+= 2) {
					if (count($params) > ($i + 1)) {
						$_GET[$params[$i]] = $params[$i + 1];
						$_REQUEST[$params[$i]] = $params[$i + 1];
					} else {
						$_GET[$params[$i]] = $params[$i];
						$_REQUEST[$params[$i]] = $params[$i];
					}
				}
			}
		}

		$arrPar = [];
		foreach ($params as $key => $val)
        {
            if($key === 0 || $key % 2 === 0) {
                $par = explode('[', $val);
                if(count($par) > 1) {
                    $par2 = explode(']',$par[1]);
                    $keyParam = $par[0];
                    $keyArray = $par2[0];
                    $arrPar[$keyParam][$keyArray] = $params[$key+1];
                }
            }
        }

		return [$pathInfo,$arrPar];
    }

    public function encode($value) {
		if (!$value) {
			return false;
		}

        $passphrase = $this->skey;
        $salt = openssl_random_pseudo_bytes(8);
        $salted = '';
        $dx = '';
        while (strlen($salted) < 48) {
            $dx = md5($dx . $passphrase . $salt, true);
            $salted .= $dx;
        }
        $key = substr($salted, 0, 32);
        $iv = substr($salted, 32, 16);
        $encryptedData = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
        $data = array("ct" => base64_encode($encryptedData), "iv" => bin2hex($iv), "s" => bin2hex($salt));

        $enc = base64_encode(json_encode($data));
        return $enc;
    }

    public function decode($value) {
		if(!$value) {
			return false;
		}

        try {

            $passphrase = $this->skey;
            $jsonString = base64_decode($value);
            $jsonData = json_decode($jsonString, true);
            $salt = hex2bin($jsonData["s"]);
            $ct = base64_decode($jsonData["ct"]);
            $iv = hex2bin($jsonData["iv"]);
            $concatedPassphrase = $passphrase . $salt;
            $md5 = array();
            $md5[0] = md5($concatedPassphrase, true);
            $result = $md5[0];
            for ($i = 1; $i < 3; $i++) {
                $md5[$i] = md5($md5[$i - 1] . $concatedPassphrase, true);
                $result .= $md5[$i];
            }
            $key = substr($result, 0, 32);
            $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
            if (!$data) return false;

            $data = ltrim($data, '"');
            $data = rtrim($data, '"');

            return $data;
        } catch (\Exception $e) {
            return false;
        }
    }

}