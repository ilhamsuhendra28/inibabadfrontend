<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\Menu;
use app\models\TrArtikel;
use app\components\Logic;
use yii\data\ActiveDataProvider;

class ListController extends Controller
{
    public function actionIndex($name)
    {
		$menu = Menu::find()->andWhere('slug_name = :param1', [':param1'=>$name])->one();
		
		$artikel = TrArtikel::find()->andWhere('menu_id = :param1', [':param1'=>$menu->id])->orderBy(['id'=>SORT_DESC])->one();
		$expjudul = explode(' ',$artikel->judul);
		$impjudul = implode(',',$expjudul);
		$url = explode('embed/', Logic::videoPertama($artikel->isi));
		if($menu->is_video == true){
			\Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => $menu->name]);
			
		}else{
			\Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => $menu->name.' - '.$artikel->judul]);			
		}
		\Yii::$app->view->registerMetaTag(['property' => 'fb:app_id', 'content' => '884284381724333']);
		\Yii::$app->view->registerMetaTag(['property' => 'og:url', 'content' => Yii::$app->request->url]);
		\Yii::$app->view->registerMetaTag(['property' => 'og:type', 'content' => 'article']);
		\Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => substr(strip_tags(Logic::removeNbsp($artikel->isi)), 0, 150)]);
		\Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => substr(strip_tags(Logic::removeNbsp($artikel->isi)), 0, 150)]);
		\Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $impjudul.' (Berita terbaru, berita terkini, berita viral, berita kisaran, berita medan, berita sumut, berita sumatera utara, berita kriminal, berita kesehatan, berita antariksa, berita olahraga, berita travelling, video youtube, berita bisnis, berita  kesehatan, berita di indonesia dan internasional, nurkarim nehe, asmi group, jernih memandang sekitar, tempat kita jumpa)']);
		
		if(Logic::gambarPertama($artikel->isi) == ''){
            if(Logic::videoPertama($artikel->isi) != ''){
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:url', 'content' => Logic::videoPertama($artikel->isi)]);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:secure_url', 'content' => Logic::videoPertama($artikel->isi)]);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:type', 'content' => 'text/html']);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:width', 'content' => '1280']);
				\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => '780']);
            }else{
				\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => 'http://inibabad.com/themes/customilham/assets/imgs/logoinibabad.png']);
            }
        }else{
			if(Logic::videoPertama($artikel->isi) == ''){
				\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => Logic::gambarPertama($artikel->isi)]);
			}else{
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:url', 'content' => Logic::videoPertama($artikel->isi)]);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:secure_url', 'content' => Logic::videoPertama($artikel->isi)]);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:type', 'content' => 'text/html']);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:width', 'content' => '1280']);
				\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => 'https://i.ytimg.com/vi/'.$url[1].'/hqdefault.jpg']);
			}
        }
		
		if($menu->is_video == true){
			$dataProvider = new ActiveDataProvider([
				'query' => TrArtikel::find()
					->andWhere(['like', 'isi', '%iframe%', false]),
				'pagination' => [
					'pageSize' => 10,
				],
				'sort' => [
					'defaultOrder' => [
						'id' => SORT_DESC
					]
				],
			]);
		}else{
			$dataProvider = new ActiveDataProvider([
				'query' => TrArtikel::find()->andWhere('menu_id = :param1', [':param1'=>$menu->id]),
				'pagination' => [
					'pageSize' => 10,
				],
				'sort' => [
					'defaultOrder' => [
						'id' => SORT_DESC
					]
				],
			]);
		}	
		
        return $this->render('index', [
			'dataProvider'=>$dataProvider,
			'menu'=>$menu
		]);
    }
}
