<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\TrArtikel;
use app\components\Logic;
use yii\data\ActiveDataProvider;

class ArsipController extends Controller
{
	public function actionIndex($tahun, $bulan)
    {	
		strip_tags($tahun);
		strip_tags($bulan);
		
		$checktahun = TrArtikel::find()->andWhere('YEAR(created_date) = :param1', [':param1'=>$tahun,])->one();
		if(empty($checktahun)){
			 throw new \yii\web\HttpException(404, 'The page you were looking for could not be found. You may have typed the address incorrectly or you may have used an outdated link, or that page is traveling on Mars :).');
		}
		
		
		$check = TrArtikel::find()->andWhere('YEAR(created_date) = :param1 AND MONTH(created_date) = :param2', [':param1'=>$tahun, ':param2'=>$bulan])->one();
		if(!empty($check)){
			$bulanindo = Logic::getIndoMonth($bulan);
			
			$artikel = TrArtikel::find()->andWhere('YEAR(created_date) = :param1 AND MONTH(created_date) = :param2', [':param1'=>$tahun, ':param2'=>$bulan])->orderBy(['id'=>SORT_DESC])->one();
			
			$expjudul = explode(' ',$artikel->judul);
			$impjudul = implode(',',$expjudul);
			$url = explode('embed/', Logic::videoPertama($artikel->isi));
			
			\Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => 'Arsip '.$bulanindo.' '.$tahun.' - '.$artikel->judul]);			
			\Yii::$app->view->registerMetaTag(['property' => 'fb:app_id', 'content' => '884284381724333']);;
			\Yii::$app->view->registerMetaTag(['property' => 'og:type', 'content' => 'article']);
			\Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => substr(strip_tags(Logic::removeNbsp($artikel->isi)), 0, 150)]);
			\Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => substr(strip_tags(Logic::removeNbsp($artikel->isi)), 0, 150)]);
			\Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $impjudul.' (Berita terbaru, berita terkini, berita viral, berita kisaran, berita medan, berita sumut, berita sumatera utara, berita kriminal, berita kesehatan, berita antariksa, berita olahraga, berita travelling, video youtube, berita bisnis, berita  kesehatan, berita di indonesia dan internasional, nurkarim nehe, asmi group, jernih memandang sekitar, tempat kita jumpa)']);
			
			if(Logic::gambarPertama($artikel->isi) == ''){
				if(Logic::videoPertama($artikel->isi) != ''){
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:url', 'content' => Logic::videoPertama($artikel->isi)]);
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:secure_url', 'content' => Logic::videoPertama($artikel->isi)]);
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:type', 'content' => 'text/html']);
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:width', 'content' => '1280']);
					\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => '780']);
				}else{
					\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => 'http://inibabad.com/themes/customilham/assets/imgs/logoinibabad.png']);
				}
			}else{
				if(Logic::videoPertama($artikel->isi) == ''){
					\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => Logic::gambarPertama($artikel->isi)]);
				}else{
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:url', 'content' => Logic::videoPertama($artikel->isi)]);
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:secure_url', 'content' => Logic::videoPertama($artikel->isi)]);
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:type', 'content' => 'text/html']);
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:width', 'content' => '1280']);
					\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => 'https://i.ytimg.com/vi/'.$url[1].'/hqdefault.jpg']);
				}
			}
			
			$dataProvider = new ActiveDataProvider([
				'query' => TrArtikel::find()->andWhere('YEAR(created_date) = :param1 AND MONTH(created_date) = :param2', [':param1'=>$tahun, ':param2'=>$bulan]),
				'pagination' => [
					'pageSize' => 10,
				],
				'sort' => [
					'defaultOrder' => [
						'id' => SORT_DESC
					]
				],
			]);
		}else{
			$artikel = TrArtikel::find()->andWhere('YEAR(created_date) = :param1', [':param1'=>$tahun])->orderBy(['id'=>SORT_DESC])->one();
			
			$expjudul = explode(' ',$artikel->judul);
			$impjudul = implode(',',$expjudul);
			$url = explode('embed/', Logic::videoPertama($artikel->isi));
			
			\Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => 'Arsip '.$tahun.' - '.$artikel->judul]);			
			\Yii::$app->view->registerMetaTag(['property' => 'fb:app_id', 'content' => '884284381724333']);;
			\Yii::$app->view->registerMetaTag(['property' => 'og:type', 'content' => 'article']);
			\Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => substr(strip_tags(Logic::removeNbsp($artikel->isi)), 0, 150)]);
			\Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => substr(strip_tags(Logic::removeNbsp($artikel->isi)), 0, 150)]);
			\Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $impjudul.' (Berita terbaru, berita terkini, berita viral, berita kisaran, berita medan, berita sumut, berita sumatera utara, berita kriminal, berita kesehatan, berita antariksa, berita olahraga, berita travelling, video youtube, berita bisnis, berita  kesehatan, berita di indonesia dan internasional, nurkarim nehe, asmi group, jernih memandang sekitar, tempat kita jumpa)']);
			
			if(Logic::gambarPertama($artikel->isi) == ''){
				if(Logic::videoPertama($artikel->isi) != ''){
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:url', 'content' => Logic::videoPertama($artikel->isi)]);
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:secure_url', 'content' => Logic::videoPertama($artikel->isi)]);
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:type', 'content' => 'text/html']);
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:width', 'content' => '1280']);
					\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => '780']);
				}else{
					\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => 'http://inibabad.com/themes/customilham/assets/imgs/logoinibabad.png']);
				}
			}else{
				if(Logic::videoPertama($artikel->isi) == ''){
					\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => Logic::gambarPertama($artikel->isi)]);
				}else{
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:url', 'content' => Logic::videoPertama($artikel->isi)]);
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:secure_url', 'content' => Logic::videoPertama($artikel->isi)]);
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:type', 'content' => 'text/html']);
					\Yii::$app->view->registerMetaTag(['property' => 'og:video:width', 'content' => '1280']);
					\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => 'https://i.ytimg.com/vi/'.$url[1].'/hqdefault.jpg']);
				}
			}
			
			$dataProvider = new ActiveDataProvider([
				'query' => TrArtikel::find()->andWhere('YEAR(created_date) = :param1', [':param1'=>$tahun]),
				'pagination' => [
					'pageSize' => 10,
				],
				'sort' => [
					'defaultOrder' => [
						'id' => SORT_DESC
					]
				],
			]);
		}
        return $this->render('index', [
			'dataProvider'=>$dataProvider,
			'tahun'=>$tahun,
			'bulanindo'=>$bulanindo
		]);
    }
	
	public function actionDatabulan()
    {
        if (Yii::$app->request->isAjax) {
            try {
				$tahun = $_POST['tahun'];
				
                $bulan = Logic::getBulan($tahun);
                return $this->renderPartial('/site/_arsipbulan', [
					'bulan'=>$bulan
				]);
            } catch (\Exception $e) {
                return $e->getMessage().'Terjadi kesalahan! Silahkan hubungi administrator';
            }
        }
    }
}
