<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\TrArtikel;
use app\components\Logic;
use yii\data\ActiveDataProvider;

class ContentController extends Controller
{
    public function actionIndex($judul)
    {
		$artikel = TrArtikel::find()->andWhere('slug_judul = :param1', [':param1'=>$judul])->one();
		$artikel->total_views = $artikel->total_views + 1;
		$artikel->save();
		
		$expjudul = explode(' ',$artikel->judul);
		$impjudul = implode(',',$expjudul);
		$url = explode('embed/', Logic::videoPertama($artikel->isi));
		
		\Yii::$app->view->registerMetaTag(['property' => 'fb:app_id', 'content' => '884284381724333']);
		\Yii::$app->view->registerMetaTag(['property' => 'og:url', 'content' => $artikel->judul]);
		\Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => $artikel->judul]);
		\Yii::$app->view->registerMetaTag(['property' => 'og:type', 'content' => 'article']);
		\Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => substr(strip_tags(Logic::removeNbsp($artikel->isi)), 0, 150)]);
		\Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => substr(strip_tags(Logic::removeNbsp($artikel->isi)), 0, 150)]);
		\Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $impjudul.' (Berita terbaru, berita terkini, berita viral, berita kisaran, berita medan, berita sumut, berita sumatera utara, berita kriminal, berita kesehatan, berita antariksa, berita olahraga, berita travelling, video youtube, berita bisnis, berita  kesehatan, berita di indonesia dan internasional, nurkarim nehe, asmi group, jernih memandang sekitar, tempat kita jumpa)']);
		
		if(Logic::gambarPertama($artikel->isi) == ''){
            if(Logic::videoPertama($artikel->isi) != ''){
				\Yii::$app->view->registerMetaTag(['property' => 'og:video', 'content' => Logic::videoPertama($artikel->isi)]);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:url', 'content' => Logic::videoPertama($artikel->isi)]);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:secure_url', 'content' => Logic::videoPertama($artikel->isi)]);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:type', 'content' => 'video/mp4']);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:width', 'content' => '1280']);
				\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => '780']);
            }else{
				\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => 'http://inibabad.com/themes/customilham/assets/imgs/logoinibabad.png']);
            }
        }else{
			if(Logic::videoPertama($artikel->isi) == ''){
				$gambarall = Logic::gambarAll($artikel->isi);
				foreach($gambarall as $gdx=>$grow){
					\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => $grow]);
				}
			}else{
				\Yii::$app->view->registerMetaTag(['property' => 'og:video', 'content' => Logic::videoPertama($artikel->isi)]);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:url', 'content' => Logic::videoPertama($artikel->isi)]);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:secure_url', 'content' => Logic::videoPertama($artikel->isi)]);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:type', 'content' => 'video/mp4']);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:width', 'content' => '1280']);
				\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => 'https://i.ytimg.com/vi/'.$url[1].'/hqdefault.jpg']);
			}
        }
		
        return $this->render('index', [
			'artikel'=>$artikel
		]);
    }
}
