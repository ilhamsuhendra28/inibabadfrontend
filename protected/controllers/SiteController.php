<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\TrArtikel;
use app\models\Menu;
use app\components\Logic;
use app\components\Api;

class SiteController extends Controller
{
	public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }	

    public function actionIndex()
    {
		$as = TrArtikel::find()->orderBy(['id'=>SORT_DESC])->one();
		$expjudul = explode(' ',$as->judul);
		$impjudul = implode(',',$expjudul).' (Berita terbaru, berita terkini, berita viral, berita kisaran, berita medan, berita sumut, berita sumatera utara, berita kriminal, berita kesehatan, berita antariksa, berita olahraga, berita travelling, video youtube, berita bisnis, berita  kesehatan, berita di indonesia dan internasional, nurkarim nehe, asmi group, jernih memandang sekitar, tempat kita jumpa)';
		$url = explode('embed/', Logic::videoPertama($as->isi));
		
		\Yii::$app->view->registerMetaTag(['property' => 'fb:app_id', 'content' => '884284381724333']);
		\Yii::$app->view->registerMetaTag(['property' => 'og:url', 'content' => Yii::$app->request->url]);
		\Yii::$app->view->registerMetaTag(['property' => 'og:title', 'content' => $as->judul]);
		\Yii::$app->view->registerMetaTag(['property' => 'og:type', 'content' => 'article']);
		\Yii::$app->view->registerMetaTag(['property' => 'og:description', 'content' => substr(strip_tags(Logic::removeNbsp($as->isi)), 0, 150)]);
		\Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => substr(strip_tags(Logic::removeNbsp($as->isi)), 0, 150)]);
		\Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => $impjudul]);
		
		if(Logic::gambarPertama($as->isi) == ''){
            if(Logic::videoPertama($as->isi) != ''){
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:url', 'content' => Logic::videoPertama($as->isi)]);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:secure_url', 'content' => Logic::videoPertama($as->isi)]);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:type', 'content' => 'text/html']);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:width', 'content' => '1280']);
				\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => '780']);
            }else{
				\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => 'http://inibabad.com/themes/customilham/assets/imgs/logoinibabad.png']);
            }
        }else{
			if(Logic::videoPertama($as->isi) == ''){
				\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => Logic::gambarPertama($as->isi)]);
			}else{
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:url', 'content' => Logic::videoPertama($as->isi)]);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:secure_url', 'content' => Logic::videoPertama($as->isi)]);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:type', 'content' => 'text/html']);
				\Yii::$app->view->registerMetaTag(['property' => 'og:video:width', 'content' => '1280']);
				\Yii::$app->view->registerMetaTag(['property' => 'og:image', 'content' => 'https://i.ytimg.com/vi/'.$url[1].'/hqdefault.jpg']);
			}
        }
		
        return $this->render('index');
    }
	
	public function actionLatestvideo()
    {
		$name = $_POST['menu'];
		if(!empty($name)){
			$menu = Menu::find()->andWhere('slug_name = :param1', [':param1'=>$name])->one();
			if(empty($menu)){
				 throw new \yii\web\HttpException(404, 'The page you were looking for could not be found. You may have typed the address incorrectly or you may have used an outdated link, or that page is traveling on Mars :).');
			}
		}
		
        if (Yii::$app->request->isAjax) {
            try {
                return $this->renderPartial('_latestvideo', [
					'menu'=>$menu
				]);
            } catch (\Exception $e) {
                return $e->getMessage().'Terjadi kesalahan! Silahkan hubungi administrator';
            }
        }
    }
	
	public function actionLatestpost()
    {
        if (Yii::$app->request->isAjax) {
            try {
                return $this->renderPartial('_latestpost');
            } catch (\Exception $e) {
                return $e->getMessage().'Terjadi kesalahan! Silahkan hubungi administrator';
            }
        }
    }
	
	public function actionPopulerpost()
    {
		$name = $_POST['menu'];
		if(!empty($name)){
			$menu = Menu::find()->andWhere('slug_name = :param1', [':param1'=>$name])->one();
			if(empty($menu)){
				 throw new \yii\web\HttpException(404, 'The page you were looking for could not be found. You may have typed the address incorrectly or you may have used an outdated link, or that page is traveling on Mars :).');
			}
		}
		
        if (Yii::$app->request->isAjax) {
            try {
                return $this->renderPartial('_populerpost', [
					'menu'=>$menu
				]);
            } catch (\Exception $e) {
                return $e->getMessage().'Terjadi kesalahan! Silahkan hubungi administrator';
            }
        }
    }
	
	public function actionCorona()
    {
        if (Yii::$app->request->isAjax) {
            try {
				$negara = $_POST['negara'];
				strip_tags($negara);
				$apisingle = Api::datacoronasingle($negara);
				$apiall = Api::datacoronaall();
				$dataall = [];
				foreach($apiall as $adx=>$arow){
					$dataall['cases'] += $arow['cases'];
					$dataall['todayCases'] += $arow['todayCases'];
					$dataall['deaths'] += $arow['deaths'];
					$dataall['todayDeaths'] += $arow['todayDeaths'];
					$dataall['recovered'] += $arow['recovered'];
					$dataall['todayRecovered'] += $arow['todayRecovered'];
					$dataall['active'] += $arow['active'];
				}
				
                return $this->renderPartial('_corona', [
					'apisingle'=>$apisingle,
					'dataall'=>$dataall
				]);
            } catch (\Exception $e) {
                return $e->getMessage().'Terjadi kesalahan! Silahkan hubungi administrator';
            }
        }
    }
	
	public function actionLoadvideo()
    {
        if (Yii::$app->request->isAjax) {
            try {
				$dataurl = $_POST['dataurl'];
				strip_tags($dataurl);
				
                return $this->renderPartial('_loadvideo', [
					'dataurl'=>$dataurl
				]);
            } catch (\Exception $e) {
                return $e->getMessage().'Terjadi kesalahan! Silahkan hubungi administrator';
            }
        }
    }
}
